import requests
import os
from dotenv import load_dotenv
load_dotenv()
token = os.getenv('token')
user_choice = input("Available options are : project/file. If a project doesnt exist yet, please start with that" + " ")

if user_choice == "project":
    project_name = input("What should be the name of the project?" + " ")
    create_project_request = requests.post(
        url = 'https://gitlab.com/api/v4/projects/',
        headers={
            "Content-Type":"application/json",
            "PRIVATE-TOKEN":token},
        params={
            "namespace_id": "15247495",
            "name": project_name,
            "description":"this is a simple project to exercise working with APIs"})

elif user_choice == "file":
    file_name = input("What should be the name of the file?" + " ")
    project_id = input("Also insert the id of the project in which the file will be" + " ")
    create_new_file = requests.post(
        url = f"https://gitlab.com/api/v4/projects/{project_id}/repository/files/dir_for_test%2F{file_name}%2E",
        headers={
            "Content-Type":"application/json",
            "PRIVATE-TOKEN":token},
        params={
            "branch":"master",
            "author_email":"cristiansuciuc2324@gmail.com",
            "author_name":"Cristian Suciuc",
            "content":"this is a new file created for testing purposes",
            "commit_message":"created a new file"})

else:
    print("no input/wrong input given")